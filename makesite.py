#!/usr/bin/env python3

# The MIT License (MIT)
#
# Copyright (c) 2018 Sunaina Pai
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


"""Make static website/blog with Python."""


import os
import shutil
import re
import glob
import sys
import json
import datetime


def fread(filename):
    """Read file and close the file."""
    with open(filename, 'r') as f:
        return f.read()


def fwrite(filename, text):
    """Write content to file and close the file."""
    basedir = os.path.dirname(filename)
    if not os.path.isdir(basedir):
        os.makedirs(basedir)

    with open(filename, 'w') as f:
        f.write(text)


def log(msg, *args):
    """Log message with specified arguments."""
    sys.stderr.write(msg.format(*args) + '\n')


def truncate(text, words=36):
    """Remove tags and truncate text to the first few sentences, up to the specified number of words."""
    count = 0
    processedtext = re.sub('(?s)<.*?>', ' ', re.sub('<h.?>.*</h.?>', '', text)).replace(' .', '.')
    for sentence in processedtext.split('.'):
        length = len(sentence.split())
        if count + length < words:
            count += length
        else:
            count = words if count == 0 else count
            break
    truncation = ' '.join(processedtext.split()[:count])
    if len(truncation):
        return truncation[:-1] if truncation[-1] == '.' else truncation
    else:
        return ''


def read_headers(text):
    """Parse headers in text and yield (key, value, end-index) tuples."""
    for match in re.finditer(r'\s*<!--\s*(.+?)\s*:\s*(.+?)\s*-->\s*|.+', text):
        if not match.group(1):
            break
        yield match.group(1), match.group(2), match.end()


def rfc_2822_format(date_str):
    """Convert yyyy-mm-dd date string to RFC 2822 format date string."""
    d = datetime.datetime.strptime(date_str, '%Y-%m-%d')
    return d.strftime('%a, %d %b %Y %H:%M:%S +0000')

def read_content(filename):
    """Read content and metadata from file into a dictionary."""
    # Read file content.
    text = fread(filename)

    # Read metadata and save it in a dictionary.
    date_slug = os.path.basename(filename).split('.')[0]
    match = re.search(r'^(?:(\d\d\d\d-\d\d-\d\d)-)?(.+)$', date_slug)
    content = {
        'date': match.group(1) or '1970-01-01',
        'slug': match.group(2).lower(),
    }

    # Read headers.
    end = 0
    for key, val, end in read_headers(text):
        content[key] = val

    # Separate content from headers.
    text = text[end:]
    toc = ''

    # Convert Markdown / txt content to HTML.
    if filename.endswith(('.md', '.mkd', '.mkdn', '.mdown', '.markdown')):

        codehighlighting.reset_toc()
        text = markdown_parser.parse(text)

        try:
            toc = codehighlighting.render_toc(level=4)
        except:
            toc = ''
        if len(toc) > 1:
            toc = ('<h5>' + content['title'] + '</h5>\n' + toc)

    # Update the dictionary with content and RFC 2822 date.
    content.update({
        'content': text,
        'toc': toc,
        'rfc_2822_date': rfc_2822_format(content['date'])
    })

    return content


def render(template, **params):
    """Replace placeholders in template with values from params."""
    return re.sub(r'{{\s*([^}\s]+)\s*}}',
                  lambda match: str(params.get(match.group(1), match.group(0))),
                  template)


def make_pages(src, dst, layout, **params):
    """Generate pages from page content."""
    items = []

    for src_path in glob.glob(src):
        content = read_content(src_path)

        page_params = dict(params, **content)

        # Encrypt page content if useful
        if 'passphrase' in page_params:
            if 'passphrase_hint' not in page_params:
                page_params.update({
                    'passphrase_hint': ''
                })
            else:
                page_params['passphrase_hint'] = "Hint: {}".format(page_params['passphrase_hint'])
            text = passphrase_protect(page_params)
            page_params.update({
                'content': text,
                'toc': ''
            })
        else:
            if len(page_params['toc']) > 1:
                page_params['toc'] = '<nav>\n' + page_params['toc'] + '</nav>\n'

        if 'description' not in page_params:
            page_params['description'] = truncate(page_params['content'], words=24).replace('"', "'") + '...'

        if 'page_url' not in page_params:
            page_slug = page_params['slug'] if page_params['slug'] != '_index' else ''
            page_path = (page_params['blog'] + '/' if 'blog' in page_params else '') + page_slug
            page_url = page_params['base_path'] + page_path + '/' if len(page_path) > 0 else ''
            page_params['page_url'] = '/'.join((page_params['site_url'], page_url))

        # Populate placeholders in content if content-rendering is enabled.
        if page_params.get('render') == 'yes':
            rendered_content = render(page_params['content'], **page_params)
            page_params['content'] = rendered_content
            content['content'] = rendered_content

        if 'blog' in page_params:
            content['blog'] = page_params['blog']

        items.append(content)

        dst_path = render(dst, **page_params)
        output = render(layout, **page_params)

        log('Rendering {} => {} ...', src_path, dst_path)
        fwrite(dst_path, output)

    return sorted(items, key=lambda x: x['date'], reverse=True)


def make_list(posts, dst, list_layout, item_layout, **params) -> str:
    """Generate list page for a blog."""
    items = []
    for post in posts:
        item_params = dict(params, **post)
        item_params['summary'] = truncate(post['content'])
        item = render(item_layout, **item_params)
        items.append(item)

    params['content'] = ''.join(items)
    params['slug'] = params['blog'] if 'blog' in params else params['title'].lower().replace(' ', '')
    params['description'] = "Posts from " + params['subtitle'] + "'s " + params['title'].title()
    if 'page_url' not in params:
        page_slug = params['slug'] if params['slug'] != '_index' else ''
        page_url = params['base_path'] + page_slug + '/' if len(page_slug) > 0 else ''
        params['page_url'] = '/'.join((params['site_url'], page_url))
    dst_path = render(dst, **params)
    output = render(list_layout, **params)
    
    if len(dst_path):
        log('Rendering list => {} ...', dst_path)
        fwrite(dst_path, output)
        return ''
    else:
        return output


def main():
    # Create a new _site directory from scratch.
    if os.path.isdir('_site'):
        shutil.rmtree('_site')
    shutil.copytree('static', '_site')

    # Remove any existing draft posts.
    if os.path.isdir('_draft'):
        shutil.rmtree('_draft')

    # Default parameters.
    params = {
        'base_path': '',
        'subtitle': 'Lorem Ipsum',
        'author': 'Admin',
        'site_url': 'http://localhost:8000',
        'current_year': datetime.datetime.now().year
    }

    # If params.json exists, load it.
    if os.path.isfile('params.json'):
        params.update(json.loads(fread('params.json')))

    # Load layouts.
    page_layout = fread('layout/page.html')
    post_layout = fread('layout/post.html')
    list_layout = fread('layout/list.html')
    latest_layout = fread('layout/latest.html')
    item_layout = fread('layout/item.html')
    feed_xml = fread('layout/feed.xml')
    item_xml = fread('layout/item.xml')

    # Combine layouts to form final layouts.
    post_layout = render(page_layout, content=post_layout)
    list_layout = render(page_layout, content=list_layout)

    # Create blogs.
    blog_posts = make_pages('content/blog/*.md',
                            '_site/blog/{{ slug }}/index.html',
                            post_layout, blog='blog', **params)

    # Create blog list pages.
    make_list(blog_posts, '_site/blog/index.html',
              list_layout, item_layout, blog='blog', title='Blog', **params)

    # Collate most recent 3 posts.
    latest_list = sorted(blog_posts, key=lambda x: x['date'], reverse=True)[0:3]
    latest_posts = make_list(latest_list, '',
                             latest_layout, item_layout, title='Latest Posts', **params)

    # Create site pages.
    make_pages('content/_index.html', '_site/index.html',
               page_layout, latest=latest_posts, **params)
    make_pages('content/[!_]*.html', '_site/{{ slug }}/index.html',
               page_layout, latest=latest_posts, **params)
    make_pages('content/error/[!_]*.html', '_site/error/{{ slug }}.html',
               page_layout, **params)

    # Create RSS feeds.
    make_list(blog_posts, '_site/blog/rss.xml',
              feed_xml, item_xml, blog='blog', title='Blog', **params)

    # Generate draft post previews, with accompanying list.
    if 'MAKESITE_DRAFT' in os.environ:
        _draft = os.environ['MAKESITE_DRAFT']

        # Create draft previews.
        draft_posts = make_pages('content/draft/*.md',
                                '_draft/{{ slug }}/index.html',
                                post_layout, blog=_draft, **params)

        # Create draft preview list page.
        make_list(draft_posts, '_draft/index.html',
                  list_layout, item_layout, blog=_draft, title='Draft Posts', **params)

        # Create symlink to secret hosted directory name.
        os.symlink('../_draft', '_site/' + _draft)


# Test parameter to be set temporarily by unit tests.
_test = None

def imports():
    global markdown_parser, codehighlighting, passphrase_protect

    # import global markdown parser instance with highlighting
    try:
        if _test == 'ImportError-mistune':
            raise ImportError('Error forced by test')
        import mistune

        try:
            if _test == 'ImportError-pygments':
                raise ImportError('Error forced by test')
            from pygments import highlight
            from pygments.lexers import get_lexer_by_name
            from pygments.formatters import html

            from toc import TocMixin

            class HighlightRenderer(TocMixin, mistune.Renderer):
                def block_code(self, code, lang):
                    title = ''
                    if code.split('\n')[0].startswith('title: '):
                        title = code.split('\n')[0].replace('title: ', '')
                        code = '\n'.join(code.split('\n')[1:])
                    if not lang:
                        return '\n<pre><code>%s</code></pre>\n' % \
                            mistune.escape(code)
                    lexer = get_lexer_by_name(lang, stripall=True)

                    html_formatter = html.HtmlFormatter(filename=title)
                    return highlight(code, lexer, html_formatter)

                def image(self, src, title, text):
                    return super().image(src, text if title is None else title, text)

            codehighlighting = HighlightRenderer()
            markdown_parser = mistune.Markdown(renderer=codehighlighting, parse_block_html=True)

        except ImportError as e:
            log('WARNING: Cannot highlight Markdown: {}', str(e))
            markdown_parser = mistune.Markdown(parse_block_html=True)

    except ImportError as e:
        log('WARNING: Cannot render Markdown: {}', str(e))
        markdown_parser = lambda: None
        markdown_parser.parse = lambda x: x
        codehighlighting = lambda: None
        codehighlighting.reset_toc = lambda: None
        codehighlighting.render_toc = lambda x: None
        # Run the above lambdas for test coverage
        markdown_parser()
        codehighlighting()
        codehighlighting.render_toc(None)


    # create encryption function
    # attribution: https://gist.github.com/tscholl2/dc7dc15dc132ea70a98e8542fefffa28
    try:
        if _test == 'ImportError-cryptography':
            raise ImportError('Error forced by test')
        import base64
        import hashlib
        from binascii import hexlify
        from cryptography.hazmat.primitives.ciphers.aead import AESGCM

        def deriveKey(passphrase: str, salt: bytes=None) -> [str, bytes]:
            if salt is None:
                salt = os.urandom(8)
            return hashlib.pbkdf2_hmac("sha256", passphrase.encode("utf8"), salt, 1000), salt

        def encrypt(passphrase: str, plaintext: str) -> str:
            if _test == 'Encryption':
                iv = b'A' * 12
                key, salt = deriveKey(passphrase, iv)
            else:
                iv = os.urandom(12)
                key, salt = deriveKey(passphrase)
            aes = AESGCM(key)
            plaintext = plaintext.encode("utf8")
            ciphertext = aes.encrypt(iv, plaintext, None)
            return "%s-%s-%s" % (hexlify(salt).decode("utf8"), hexlify(iv).decode("utf8"), hexlify(ciphertext).decode("utf8"))

        def passphrase_protect(page_params: dict) -> str:
            template  = fread('layout/encrypted.html')
            result    = encrypt(page_params['passphrase'], page_params['content'])
            if len(page_params['toc']) > 0:
                resulttoc = encrypt(page_params['passphrase'], page_params['toc'])
            else:
                resulttoc = ''
            page_params.update({
                'encryptedcontent': result,
                'encryptedtoc': resulttoc
            })
            rendered = render(template, **page_params)
            return rendered

    except ImportError as e:
        log('WARNING: Cannot encrypt content: {}', str(e))

        def passphrase_protect(page_params: dict) -> str:
            return "Content is not available due to a generator issue with the Python cryptography module. Please try again later."

imports()
if __name__ == '__main__':
    main()
