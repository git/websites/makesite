window.decryptionCompleted = 0;
function decrypted(result) {
	var container = document.createElement("section");
	container.innerHTML = result;
	window.enc.insertAdjacentElement('beforebegin', container);
	window.enc.remove();
	checkScrollSpy();
}
function decryptedtoc(result) {
	var container = document.createElement("nav");
	container.innerHTML = result;
	document.getElementsByTagName("main")[0].insertAdjacentElement('afterbegin', container);
	checkScrollSpy();
}
function checkScrollSpy() {
	window.decryptionCompleted += 1;
	if (window.decryptionCompleted > 1 && document.querySelector('main > nav'))
		window.initScrollSpy();
}

function decryptionFailed(error) {
	window.alert("The supplied passphrase is incorrect");
}

function attemptDecryption(passphrase) {
	if (!window.crypto || !window.crypto.subtle) {
		alert("Your browser does not support the Web Cryptography API! Please use a recent version of either Firefox or Chrome.");
		return;
	}
	if (!passphrase || passphrase.length < 1) return;
	var decryption = decrypt(passphrase, window.encryptedcontent);
	decryption.then(decrypted, decryptionFailed);
	if (window.encryptedtoc.length > 1) {
		var decryptiontoc = decrypt(passphrase, window.encryptedtoc);
		decryptiontoc.then(decryptedtoc, function(){});
	} else {
		window.decryptionCompleted += 1;
	}
	return decryption;
}

(function() {
	document.getElementById("nojs").remove();
	window.enc = document.getElementById("encryption");
	window.encryptedcontent = enc.dataset.content;
	window.encryptedtoc = enc.dataset.toc;
	var passphrase = document.getElementById("passphrase");
	passphrase.addEventListener("keyup", function(e) {
		if (e.keyCode === 13) { // enter key
			e.preventDefault();
			e.target.blur();
			attemptDecryption(e.target.value)
		}
	}); 
	var submitPass = document.getElementById("passphrase-submit");
	submitPass.addEventListener("click", function(e) {
		passphrase.blur();
		attemptDecryption(passphrase.value)
	}); 
})();
