(function() {
	window.el = document.getElementById("stlpreview");
	window.el.firstChild.addEventListener("click", function(e) {
		var file = e.target.dataset.file;
		window.el.removeChild(e.target);
		window.stl_viewer = new StlViewer(el, { models: [ {id:0, filename: file, color: "#f99157"} ] });
	}); 
})();
