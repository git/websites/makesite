import unittest
import os
import shutil
import makesite
from test import path

class PagesTest(unittest.TestCase):
    def setUp(self):
        self.blog_path = path.temppath('blog')
        self.site_path = path.temppath('site')
        os.makedirs(self.blog_path)

        with open(os.path.join(self.blog_path, 'foo.txt'), 'w') as f:
            f.write('Foo')
        with open(os.path.join(self.blog_path, 'bar.txt'), 'w') as f:
            f.write('Bar')
        with open(os.path.join(self.blog_path, '2018-01-01-foo.txt'), 'w') as f:
            f.write('Foo')
        with open(os.path.join(self.blog_path, '2018-01-02-bar.txt'), 'w') as f:
            f.write('Bar')
        with open(os.path.join(self.blog_path, 'header-foo.txt'), 'w') as f:
            f.write('<!-- tag: foo -->Foo')
        with open(os.path.join(self.blog_path, 'header-bar.txt'), 'w') as f:
            f.write('<!-- title: bar -->Bar')
        with open(os.path.join(self.blog_path, 'placeholder-foo.txt'), 'w') as f:
            f.write('<!-- title: foo -->{{ title }}:{{ author }}:Foo')
        with open(os.path.join(self.blog_path, 'placeholder-bar.txt'), 'w') as f:
            f.write('<!-- title: bar --><!-- render: yes -->{{ title }}:{{ author }}:Bar')
        with open(os.path.join(self.blog_path, 'crypto.md'), 'w') as f:
            f.write('<!-- title: baz --><!-- passphrase: 123456 --><!-- passphrase_hint: Common -->{{ title }}:{{ author }}:Qwe')
        with open(os.path.join(self.blog_path, 'crypto-nohint.md'), 'w') as f:
            f.write('<!-- title: baz --><!-- passphrase: 123456 -->{{ title }}:{{ author }}:Qwe')
        with open(os.path.join(self.blog_path, 'toc.md'), 'w') as f:
            f.write('<!-- title: baz -->{{ title }}:{{ author }}:Qwe\n### Rty\n\n#### Yui\n\nop\n\n##### Asdf\n\n# Ghj\n\nkl\n\n###Zxc\n\nvbn')
        with open(os.path.join(self.blog_path, 'crypto-toc.md'), 'w') as f:
            f.write('<!-- title: fab --><!-- passphrase: 123456 --><!-- passphrase_hint: Common -->{{ title }}:{{ author }}:Qwe\n### Rty\n\n#### Yui\n\nop')

    def tearDown(self):
        shutil.rmtree(self.blog_path)
        shutil.rmtree(self.site_path)

    def test_pages_undated(self):
        src = os.path.join(self.blog_path, '[fb]*.txt')
        dst = os.path.join(self.site_path, '{{ slug }}.txt')
        tpl = '<div>{{ content }}</div>'
        makesite.make_pages(src, dst, tpl)
        with open(os.path.join(self.site_path, 'foo.txt')) as f:
            self.assertEqual(f.read(), '<div>Foo</div>')
        with open(os.path.join(self.site_path, 'bar.txt')) as f:
            self.assertEqual(f.read(), '<div>Bar</div>')

    def test_pages_dated(self):
        src = os.path.join(self.blog_path, '2*.txt')
        dst = os.path.join(self.site_path, '{{ slug }}.txt')
        tpl = '<div>{{ content }}</div>'
        makesite.make_pages(src, dst, tpl)
        with open(os.path.join(self.site_path, 'foo.txt')) as f:
            self.assertEqual(f.read(), '<div>Foo</div>')
        with open(os.path.join(self.site_path, 'bar.txt')) as f:
            self.assertEqual(f.read(), '<div>Bar</div>')

    def test_pages_layout_params(self):
        src = os.path.join(self.blog_path, '2*.txt')
        dst = os.path.join(self.site_path, '{{ slug }}.txt')
        tpl = '<div>{{ slug }}:{{ title }}:{{ date }}:{{ content }}</div>'
        makesite.make_pages(src, dst, tpl, title='Lorem')
        with open(os.path.join(self.site_path, 'foo.txt')) as f:
            self.assertEqual(f.read(), '<div>foo:Lorem:2018-01-01:Foo</div>')
        with open(os.path.join(self.site_path, 'bar.txt')) as f:
            self.assertEqual(f.read(), '<div>bar:Lorem:2018-01-02:Bar</div>')

    def test_pages_return_value(self):
        src = os.path.join(self.blog_path, '2*.txt')
        dst = os.path.join(self.site_path, '{{ slug }}.txt')
        tpl = '<div>{{ content }}</div>'
        posts = makesite.make_pages(src, dst, tpl)
        self.assertEqual(len(posts), 2)
        self.assertEqual(posts[0]['date'], '2018-01-02')
        self.assertEqual(posts[1]['date'], '2018-01-01')

    def test_content_header_params(self):
        # Test that header params from one post is not used in another
        # post.
        src = os.path.join(self.blog_path, 'header*.txt')
        dst = os.path.join(self.site_path, '{{ slug }}.txt')
        tpl = '{{ title }}:{{ tag }}:{{ content }}'
        makesite.make_pages(src, dst, tpl)
        with open(os.path.join(self.site_path, 'header-foo.txt')) as f:
            self.assertEqual(f.read(), '{{ title }}:foo:Foo')
        with open(os.path.join(self.site_path, 'header-bar.txt')) as f:
            self.assertEqual(f.read(), 'bar:{{ tag }}:Bar')

    def test_content_no_rendering(self):
        # Test that placeholders are not populated in content rendering
        # by default.
        src = os.path.join(self.blog_path, 'placeholder-foo.txt')
        dst = os.path.join(self.site_path, '{{ slug }}.txt')
        tpl = '<div>{{ content }}</div>'
        makesite.make_pages(src, dst, tpl, author='Admin')
        with open(os.path.join(self.site_path, 'placeholder-foo.txt')) as f:
            self.assertEqual(f.read(), '<div>{{ title }}:{{ author }}:Foo</div>')

    def test_content_rendering_via_kwargs(self):
        # Test that placeholders are populated in content rendering when
        # requested in make_pages.
        src = os.path.join(self.blog_path, 'placeholder-foo.txt')
        dst = os.path.join(self.site_path, '{{ slug }}.txt')
        tpl = '<div>{{ content }}</div>'
        makesite.make_pages(src, dst, tpl, author='Admin', render='yes')
        with open(os.path.join(self.site_path, 'placeholder-foo.txt')) as f:
            self.assertEqual(f.read(), '<div>foo:Admin:Foo</div>')

    def test_content_rendering_via_header(self):
        # Test that placeholders are populated in content rendering when
        # requested in content header.
        src = os.path.join(self.blog_path, 'placeholder-bar.txt')
        dst = os.path.join(self.site_path, '{{ slug }}.txt')
        tpl = '<div>{{ content }}</div>'
        makesite.make_pages(src, dst, tpl, author='Admin')
        with open(os.path.join(self.site_path, 'placeholder-bar.txt')) as f:
            self.assertEqual(f.read(), '<div>bar:Admin:Bar</div>')

    def test_rendered_content_in_summary(self):
        # Test that placeholders are populated in summary if and only if
        # content rendering is enabled.
        src = os.path.join(self.blog_path, 'placeholder*.txt')
        post_dst = os.path.join(self.site_path, '{{ slug }}.txt')
        list_dst = os.path.join(self.site_path, 'list.txt')
        post_layout = ''
        list_layout = '<div>{{ content }}</div>'
        item_layout = '<p>{{ summary }}</p>'
        posts = makesite.make_pages(src, post_dst, post_layout, author='Admin')
        makesite.make_list(posts, list_dst, list_layout, item_layout)
        with open(os.path.join(self.site_path, 'list.txt')) as f:
            self.assertEqual(f.read(), '<div><p>bar:Admin:Bar</p><p>{{ title }}:{{ author }}:Foo</p></div>')

    def test_rendered_crypto(self):
        # Test that encryption function runs and returns populated template.
        # Additionally test that encrypted output is correct.

        makesite._test = 'Encryption'

        src = os.path.join(self.blog_path, 'crypto.md')
        post_dst = os.path.join(self.site_path, '{{ slug }}.html')
        post_layout = '<h2>{{ title }} - {{ author }}</h2><div>{{ content }}</div>'
        posts = makesite.make_pages(src, post_dst, post_layout, author='Admin', base_path='/baz')

        makesite._test = None

        with open(os.path.join(self.site_path, 'crypto.html')) as f:
            crypto = f.read()

        self.assertIn('<h2>baz - Admin</h2>', crypto)
        self.assertIn('<p>Hint: Common</p>', crypto)
        self.assertIn('<section id="encryption"', crypto)
        self.assertIn('<p>This content is protected by a passphrase.</p>', crypto)
        self.assertIn('<script id="aesjs" src="/baz/js/aes.min.js"></script>', crypto)
        self.assertIn('<script src="/baz/js/encrypted.js"></script>', crypto)

        self.assertIn('data-content="414141414141414141414141-414141414141414141414141-da36ed0ba6c6265607fd73a82fc0584977045994aaf837556c350b33109be8642b972435f9e78fbbb98b231ff06b4989a85f6ff0"', crypto)

    def test_rendered_crypto_nohint(self):
        # Test that encryption function runs and returns populated template.
        # Case without hint.

        makesite._test = 'Encryption'

        src = os.path.join(self.blog_path, 'crypto-nohint.md')
        post_dst = os.path.join(self.site_path, '{{ slug }}.html')
        post_layout = '<h2>{{ title }} - {{ author }}</h2><div>{{ content }}</div>'
        posts = makesite.make_pages(src, post_dst, post_layout, author='Admin', base_path='/baz')

        makesite._test = None

        with open(os.path.join(self.site_path, 'crypto-nohint.html')) as f:
            crypto = f.read()

        self.assertIn('<h2>baz - Admin</h2>', crypto)
        self.assertIn('<p></p>', crypto)
        self.assertIn('<section id="encryption"', crypto)
        self.assertIn('<p>This content is protected by a passphrase.</p>', crypto)

        self.assertIn('data-content="414141414141414141414141-414141414141414141414141-da36ed0ba6c6265607fd73a82fc0584977045994aaf837556c350b33109be8642b972435f9e78fbbb98b231ff06b4989a85f6ff0"', crypto)

    def test_rendered_toc(self):
        # Test that Table of Contents is generated for a Markdown file with
        # headers, and that the links are correct.

        src = os.path.join(self.blog_path, 'toc.md')
        post_dst = os.path.join(self.site_path, '{{ slug }}.html')
        post_layout = '{{ toc }}\n<h2>{{ title }} - {{ author }}</h2><div>{{ content }}</div>'
        posts = makesite.make_pages(src, post_dst, post_layout, author='Admin', base_path='/fab')

        makesite._test = None

        with open(os.path.join(self.site_path, 'toc.html')) as f:
            toc = f.read()

        self.assertIn('<h5>baz</h5>', toc)
        self.assertIn('<ul id="toc">\n<li><a href="#rty">Rty</a>\n<ul>\n<li><a href="#yui">Yui</a></li>\n</ul>\n</li>\n</ul>\n</li>\n</ul>\n</li>\n<li><a href="#ghj">Ghj</a></li>\n</ul>', toc)
        self.assertIn('<h2>baz - Admin</h2>', toc)
        self.assertIn('<h3 id="rty">Rty</h3>\n<h4 id="yui">Yui</h4>\n<p>op</p>', toc)

    def test_rendered_crypto_toc(self):
        # Test that encryption function runs and returns populated template,
        # including the encrypted Table of Contents.
        # Additionally, test that encrypted output is correct.

        makesite._test = 'Encryption'

        src = os.path.join(self.blog_path, 'crypto-toc.md')
        post_dst = os.path.join(self.site_path, '{{ slug }}.html')
        post_layout = '<h2>{{ title }} - {{ author }}</h2><div>{{ content }}</div>'
        posts = makesite.make_pages(src, post_dst, post_layout, author='Admin', base_path='/baz')

        makesite._test = None

        with open(os.path.join(self.site_path, 'crypto-toc.html')) as f:
            cryptotoc = f.read()

        self.assertIn('<section id="encryption"', cryptotoc)

        self.assertIn('data-content="414141414141414141414141-414141414141414141414141-da36ed0ba6c6265607fd73a82fc0584977045994aaf837556c350b33109be8642b9724357f619e34afa8e54b1b466b128d9db31574eb5f443dbb076ffe6e5821fc753137b636aa6d3b22f143e9140c4d5b452049030cf8f3c3e89150f4a3837831928353f9ffaccfbdb7"', cryptotoc)
        self.assertIn('data-toc="414141414141414141414141-414141414141414141414141-da2ee64ebb8730035cf923b65881175e2c4d5cdcfce437446e767c352d85b36465c7724d266f9036e5beac104b0c4044caf3e80d76ce0b026f8f313ba6270f79a0772030ba72a9166d32b805a31e6b320e0931474d0cf8ef94dc395d20f3ff68cf66a5620c39bc478ec62047b26c48e70cb45c8bd98b4f2811ab754814"', cryptotoc)

