import unittest
import makesite
import os
import shutil
import json

from test import path


class MainTest(unittest.TestCase):
    _draft_backup = ""

    def setUp(self):
        self._draft_backup = os.environ['MAKESITE_DRAFT'] if 'MAKESITE_DRAFT' in os.environ else ""
        os.environ['MAKESITE_DRAFT'] = ""
        del os.environ['MAKESITE_DRAFT']
        path.move('_site', '_site.backup')
        path.move('_draft', '_draft.backup')
        path.move('params.json', 'params.json.backup')

    def tearDown(self):
        os.environ['MAKESITE_DRAFT'] = self._draft_backup
        path.move('_site.backup', '_site')
        path.move('_draft.backup', '_draft')
        path.move('params.json.backup', 'params.json')

    def test_site_missing(self):
        makesite.main()

    def test_site_exists(self):
        os.environ['MAKESITE_DRAFT'] = "nobody_will_read_this"
        os.mkdir('_site')
        os.mkdir('_draft')
        with open('_site/foo.txt', 'w') as f:
            f.write('foo')
        with open('_draft/bar.txt', 'w') as f:
            f.write('bar')

        self.assertTrue(os.path.isfile('_site/foo.txt'))
        self.assertTrue(os.path.isfile('_draft/bar.txt'))
        makesite.main()
        self.assertFalse(os.path.isfile('_site/foo.txt'))
        self.assertFalse(os.path.isfile('_draft/bar.txt'))
        del os.environ['MAKESITE_DRAFT']

    def test_default_params(self):
        makesite.main()

        with open('_site/blog/index.html') as f:
            s1 = f.read()

        with open('_site/blog/rss.xml') as f:
            s2 = f.read()

        shutil.rmtree('_site')

        self.assertIn('/">molzy</a>', s1)
        self.assertIn(' - Lorem Ipsum</title>', s1)
        self.assertIn('<p class="meta">Published on 202', s1)

        self.assertIn('<link>http://localhost:8000/</link>', s2)
        self.assertIn('<link>http://localhost:8000/blog/', s2)

    def test_json_params(self):
        params = {
            'base_path': '/base',
            'subtitle': 'Foo',
            'author': 'Bar',
            'site_url': 'http://localhost/base'
        }
        with open('params.json', 'w') as f:
            json.dump(params, f)
        makesite.main()

        with open('_site/blog/index.html') as f:
            s1 = f.read()

        with open('_site/blog/rss.xml') as f:
            s2 = f.read()

        shutil.rmtree('_site')

        self.assertIn('<a href="/base/">molzy</a>', s1)
        self.assertIn(' - Foo</title>', s1)
        self.assertIn('<p class="meta">Published on 202', s1)

        self.assertIn('<link>http://localhost/base/base/</link>', s2)
        self.assertIn('<link>http://localhost/base/base/blog/', s2)
