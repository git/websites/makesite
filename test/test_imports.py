import unittest
import shutil
import os

import makesite
from test import path

class ContentTest(unittest.TestCase):
    def _test_import_error(self, testcase):
        makesite._test = testcase
        original_log = makesite.log

        makesite.log = self.mock
        self.mock_args = None

        # Engage imports
        makesite.imports()

        makesite._test = None
        makesite.log = original_log

        # Generate all pages to test substitute lambdas
        makesite.main()

        # Reset imports
        makesite.imports()

    # Rudimentary mock because unittest.mock is unavailable in Python 2.7.
    def mock(self, *args):
        self.mock_args = args

    def test_mistune_import_error(self):
        self._test_import_error('ImportError-mistune')
        self.assertEqual(self.mock_args,
                         ('WARNING: Cannot render Markdown: {}', 'Error forced by test'))

    def test_pygments_import_error(self):
        self._test_import_error('ImportError-pygments')
        self.assertEqual(self.mock_args,
                         ('WARNING: Cannot highlight Markdown: {}', 'Error forced by test'))

    def test_cryptography_import_error(self):
        self._test_import_error('ImportError-cryptography')
        self.assertEqual(self.mock_args,
                         ('WARNING: Cannot encrypt content: {}', 'Error forced by test'))
