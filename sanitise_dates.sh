#!/usr/bin/env bash
timestamp="$(date -I) UTC"
touch _draft
find _site _draft -exec touch -m -a -d "${timestamp}" {} +
rm _draft 2>/dev/null || true
